FROM ubuntu:focal

LABEL maintainer="root@cri.epita.fr"

ARG DEBIAN_FRONTEND=noninteractive

COPY config/inn.conf /etc/news/

# hadolint ignore=DL3008
RUN apt-get update && \
    apt-get install --option=Dpkg::Options::=--force-confdef \
                    --no-install-recommends -y inn2 msmtp-mta libjson-perl \
                    libwww-perl libyaml-perl libnet-ldap-perl \
                    libmime-tools-perl rsyslog && \
    apt-get clean && rm -rf /var/lib/apt/lists/ && rm -rf /etc/news/filter && \
    mkdir -p /run/news && chown news:news /run/news && \
    mkdir -p /var/www/inn && chown news:news /var/www/inn

COPY config/ /etc/news/
COPY entrypoint.sh /app/

ENTRYPOINT ["/app/entrypoint.sh"]

EXPOSE 119 563

VOLUME /var/spool/news
VOLUME /var/lib/news
