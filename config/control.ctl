##  control.ctl - Access control for control messages.
##
##  Format:
##     <message>:<from>:<newsgroups>:<action>
##
##     <message>      Control message or "all" if it applies to all control
##                    messages.
##     <from>         Pattern that must match the From line.
##     <newsgroups>   Pattern that must match the newsgroup being newgroup'd
##                    or rmgroup'd (ignored for other messages).
##     <action>       What to do:
##                          doit        Perform action
##                          drop        Ignore message
##                          log         One line to error log
##                          mail        Send mail to admin
##                          verify-pgp_userid   Do PGP verification on user.
##                    All actions except drop and mail can be given a log
##                    location by following the action with an = and the
##                    log ("mail" says to mail the admin, an empty location
##                    tosses the log information, and a relative path xxx
##                    logs to $LOG/xxx.log).
##
##  The *last* matching entry is used.  See the expire.ctl(5) man page for
##  complete information.
##
##  This file follows the following policies:
##
##   * Most unknown or invalid control messages no longer result in mail.
##     This is due to the proliferation of forged control messages filling
##     up mailboxes.  Some news servers even get overwhelmed with trying to
##     log failure, so unsigned control messages for hierarchies that use
##     PGP are simply dropped.
##
##   * The assumption is that you now have PGP on your system.  If you
##     don't, you should get it to help protect yourself against all the
##     control message forgeries.  See <ftp://ftp.isc.org/pub/pgpcontrol/>.
##     PGP control message verification comes with all versions of INN since
##     1.5, but you will need to install either PGP or GnuPG; see the
##     installation instructions for your news server.
##
##     If for some reason you can't use PGP, search for the *PGP* comments
##     and modify the control lines to change "verify-..." in the action
##     field to "mail" or "doit=mail" or "doit=<log file>" or whatever you
##     prefer (replacing <log file> with the name of an appropriate log
##     file).
## 
##   * A number of hierarchies are for local use only but have leaked out
##     into the general stream.  In this config file, they are set so that
##     the groups will be easy to remove, and are marked with a comment of
##     *LOCAL* (for use by that organization only, not generally
##     distributed), *DEFUNCT* (a hierarchy that's no longer used), or
##     *PRIVATE* (should only be carried after making arrangements with the
##     given contact address).  Please delete all groups in those
##     hierarchies from your server if you carry them, unless you've
##     contacted the listed contact address and arranged a feed.
##
##     If you have permission to carry any of the hierarchies so listed in
##     this file, you should change the entries for those hierarchies
##     below.
##
##   * Some hierarchies are marked as *HISTORIC*.  These hierarchies
##     aren't entirely defunct, but they are very low-traffic, not widely
##     read or carried, and may not be worth carrying.  If you don't intend
##     to carry them, comment out their entries.
##
##   * Some hierarchies are marked as *RESERVED*.  These are used for
##     special purposes by news software and should not be managed by
##     control messages.  This config file drops all control messages for
##     those hierarchies.
##

## -------------------------------------------------------------------------
##	DEFAULT
## -------------------------------------------------------------------------

# Default to dropping control messages that aren't recognized to allow
# people to experiment without inadvertently mailbombing news admins.
all:*:*:drop

## -------------------------------------------------------------------------
##	NEWGROUP/RMGROUP MESSAGES
## -------------------------------------------------------------------------

## Default (for any group)
newgroup:*:*:mail
rmgroup:*:*:mail

## CONTROL (*RESERVED* -- Special hierarchy for control messages)
#
# The control.* hierarchy is reserved by RFC 5536 and MUST NOT be used
# for regular newsgroups.  It is used by some news implementations, such
# as INN, as a local, special hierarchy that shows all control messages
# posted to any group.
#
checkgroups:*:control|control.*:drop
newgroup:*:control|control.*:drop
rmgroup:*:control|control.*:drop

## EXAMPLE (*RESERVED* -- For use in examples)
#
# The example.* hierarchy is reserved by RFC 5536 and MUST NOT be used
# for regular newsgroups.  It is intended for use in examples, standards
# documents, and similar places to avoid clashes with real newsgroup
# names.
#
checkgroups:*:example|example.*:drop
newgroup:*:example|example.*:drop
rmgroup:*:example|example.*:drop

## JUNK (*RESERVED* -- Used for unwanted newsgroups)
#
# The junk newsgroup is reserved by RFC 5536 and MUST NOT be used.  It is
# used by some implementations to store messages to unwanted newsgroups.
# The junk.* hierarchy is not reserved by RFC 5536, but it's marked
# reserved here because, given the special meaning of the junk group,
# using it for any other purpose would be confusing and might trigger
# implementation bugs.
#
checkgroups:*:junk|junk.*:drop
newgroup:*:junk|junk.*:drop
rmgroup:*:junk|junk.*:drop

## -------------------------------------------------------------------------
##      CONTROL.CTL ADDITIONS
## -------------------------------------------------------------------------

# Default (for any description).
/encoding/:*:*:utf-8
