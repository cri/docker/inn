#! /usr/bin/perl -w

use lib '/usr/share/perl5';

use strict;
use vars qw(%attributes);

use Net::LDAP qw(LDAP_INVALID_CREDENTIALS);

sub getenv {
	my ($key, $default) = @_;

	return $ENV{$key} if defined($ENV{$key});
	return $default;
}

sub getsecret {
	my ($key, $default) = @_;
	return $default unless defined($ENV{$key});

	open(FH, '<', $ENV{$key}) or return $default;
	my $value = do { local $/; <FH> };
	close(FH);
	return $value =~ s/^\s+|\s+$//gr;
}

my $LDAP_URI = getenv("INN_LDAP_URI");
my $LDAP_BIND_DN = getenv("INN_LDAP_BIND_DN");
my $LDAP_BIND_PASSWORD = getsecret("INN_LDAP_BIND_PASSWORD_FILE");
my $LDAP_USER_BASE_DN = getenv("INN_LDAP_USER_BASE_DN");
my $LDAP_USER_FILTER = getenv("INN_LDAP_USER_FILTER", "(uid=%u)");
my $LDAP_USER_SCOPE = getenv("INN_LDAP_USER_SCOPE", "subtree");

# These codes are a widely implemented de facto standard.
my %authcodes = ('allowed' => 281, 'denied' => 481, 'error' => 403);

sub authenticate {
	my $user = $attributes{'username'};
	my $pass = $attributes{'password'};

	return ($authcodes{error}, "Incorrect LDAP configuration")
		unless defined($LDAP_URI) and defined($LDAP_USER_BASE_DN);

	my $ldap = Net::LDAP->new(
		$LDAP_URI,
		timeout => 5,
		onerror => 'undef',
	) or return ($authcodes{error}, "Unable to reach LDAP server");

	my $msg;
	if (defined($LDAP_BIND_DN) and defined($LDAP_BIND_PASSWORD)) {
		$msg = $ldap->bind(
			$LDAP_BIND_DN,
			password => $LDAP_BIND_PASSWORD,
		) or return (
			$authcodes{error},
			"Unable to bind to LDAP server",
		);
	} else {
		$msg = $ldap->bind()
			or return (
				$authcodes{error},
				"Unable to bind to LDAP server"
			);
	}

	return ($authcodes{error}, "Unable to bind to LDAP server")
		if $msg->is_error();

	my $srch = $ldap->search(
		base => $LDAP_USER_BASE_DN,
		filter => $LDAP_USER_FILTER =~ s/%u/$user/r,
		scope => $LDAP_USER_SCOPE,
		sizelimit => 2,
	) or return ($authcodes{"error"}, "Unable to get user DN");

	return ($authcodes{"error"}, "Unable to get user DN")
		if $srch->is_error() or $srch->count() > 1;

	return ($authcodes{"denied"}, "User is not allowed")
		if $srch->count() == 0;

	my $user_dn = $srch->pop_entry()->dn();

	$msg = $ldap->bind(
		$user_dn,
		password => $pass,
	) or return (
		$authcodes{error},
		"Unable to bind to LDAP server as user",
	);

	return ($authcodes{denied}, "Incorrect user/password.")
		if $msg->code == &LDAP_INVALID_CREDENTIALS;

	return ($authcodes{error}, "Unable to bind to LDAP server as user")
		if $msg->is_error();

	return ($authcodes{allowed}, "");
}
