#!/usr/bin/env perl

use utf8;
use Encode;
use JSON ();
use HTTP::Request ();
use LWP::UserAgent ();
use MIME::Parser;
use MIME::WordDecoder;


sub filter_art {
    my $ua = LWP::UserAgent->new;
    $ua->timeout(2);

    my @endpoints = split(" ", $ENV{'INN_WEBHOOK_ENDPOINTS'});

    my $message = "";

    foreach $header (keys %hdr) {
        if ($header ne "__BODY__") {
            $message .= $header . ": " . $hdr{$header};
	    $message .= "\r\n";
        }
    }

    if (exists($hdr{"__BODY__"})) {
        $message .= "\r\n" . $hdr{"__BODY__"};
    }

    my $parser = new MIME::Parser;
    $parser->output_to_core(1);
    $parser->decode_bodies(1);
    my $entity = eval { $parser->parse_data($message); };

    my %data = %hdr;
    if (defined($entity)) {
        my $head = $entity->head;
        foreach ($head->tags) {
            eval {
                $data{$_} = mime_to_perl_string($head->get($_, 0));
                chomp($data{$_});
            }
        }
        my $bodyh = $entity->bodyhandle;
        if ($bodyh and $entity->effective_type eq "text/plain") {
            $data{"__BODY__"} = $bodyh->as_string;
            eval {
                $data{"__BODY__"} = decode(
                    $head->mime_attr("content-type.charset"),
                    $data{"__BODY__"},
                );
            };
	    $data{"Content-Type"} = "text/plain; charset=UTF-8";
	    $data{"Content-Transfer-Encoding"} = "binary";
        }
    }

    chomp($data{"__BODY__"});
    chop($data{"__BODY__"}) if (substr($data{"__BODY__"}, -1) eq ".");

    for (@endpoints) {
        my $request = HTTP::Request->new(
            "POST",
            $_,
            ['Content-Type' => 'application/json'],
            JSON::encode_json(\%data),
        );
        my $res = $ua->request($request);
    }

    return "";
}
