#!/usr/bin/env perl

use lib '/usr/share/perl5';

use strict;
use vars qw(%attributes);

use YAML ();
use Net::LDAP qw(LDAP_INVALID_CREDENTIALS);

sub getenv {
	my ($key, $default) = @_;

	return $ENV{$key} if defined($ENV{$key});
	return $default;
}

sub getsecret {
	my ($key, $default) = @_;
	return $default unless defined($ENV{$key});

	open(FH, '<', $ENV{$key}) or return $default;
	my $value = do { local $/; <FH> };
	close(FH);
	return $value =~ s/^\s+|\s+$//gr;
}

my $LDAP_URI = getenv("INN_LDAP_URI");
my $LDAP_BIND_DN = getenv("INN_LDAP_BIND_DN");
my $LDAP_BIND_PASSWORD = getsecret("INN_LDAP_BIND_PASSWORD_FILE");
my $LDAP_USER_BASE_DN = getenv("INN_LDAP_USER_BASE_DN");
my $LDAP_USER_FILTER = getenv("INN_LDAP_USER_FILTER", "(uid=%u)");
my $LDAP_USER_SCOPE = getenv("INN_LDAP_USER_SCOPE", "subtree");
my $LDAP_GROUP_BASE_DN = getenv("INN_LDAP_GROUP_BASE_DN", $LDAP_USER_BASE_DN);
my $LDAP_GROUP_FILTER = getenv(
	"INN_LDAP_GROUP_FILTER",
	"(|(member=%d)(memberUid=%u))"
);
my $LDAP_GROUP_SCOPE = getenv("INN_LDAP_GROUP_SCOPE", $LDAP_USER_SCOPE);
my $LDAP_GROUP_NAME_ATTR = getenv("INN_LDAP_GROUP_NAME_ATTR", "cn");
my $LDAP_GROUP_MAP_PATH = getenv("INN_LDAP_GROUP_MAP");

sub get_groups {
	my ($user) = @_;

	return undef
		unless defined($LDAP_URI) and defined($LDAP_USER_BASE_DN);

	my $ldap = Net::LDAP->new(
		$LDAP_URI,
		timeout => 5,
		onerror => 'undef',
	) or return undef;

	my $msg;
	if (defined($LDAP_BIND_DN) and defined($LDAP_BIND_PASSWORD)) {
		$msg = $ldap->bind(
			$LDAP_BIND_DN,
			password => $LDAP_BIND_PASSWORD,
		) or return undef;
	} else {
		$msg = $ldap->bind() or return undef;
	}

	return undef if $msg->is_error();

	my $srch = $ldap->search(
		base => $LDAP_USER_BASE_DN,
		filter => $LDAP_USER_FILTER =~ s/%u/$user/r,
		scope => $LDAP_USER_SCOPE,
		sizelimit => 2,
	) or return undef;

	return undef if $srch->is_error() or $srch->count() > 1;
	return undef if $srch->count() == 0;

	my $user_dn = $srch->pop_entry()->dn();

	my @groups = ();
	if (defined($LDAP_GROUP_MAP_PATH))
	{
		my $group_filter = $LDAP_GROUP_FILTER =~ s/%u/$user/r;
		$group_filter =~ s/%d/$user_dn/;

		$srch = $ldap->search(
			base => $LDAP_GROUP_BASE_DN,
			scope => $LDAP_GROUP_SCOPE,
			filter => $group_filter,
			attrs => [$LDAP_GROUP_NAME_ATTR],
		) or return undef;

		return undef if $srch->is_error();

		foreach my $entry ($srch->entries()) {
			push(@groups, $entry->get_value($LDAP_GROUP_NAME_ATTR));
		}
	}
	return @groups;
}


sub load_group_map {
	my ($group_map_path) = @_;
	my %default_map = (
		"_default" => {
			read => "!*",
			post => "!*",
		},
	);
	return %default_map unless defined($group_map_path);
	my %map = %{ YAML::LoadFile($group_map_path) };
	return (%default_map, %map)
}

sub access {
	my %return_hash = (
		"backoff_auth" => "true",
		"nnrpdauthsender" => "true",
		"addinjectionpostingaccount" => "true",
	);

	my %group_map = load_group_map($LDAP_GROUP_MAP_PATH);
	my $read = $group_map{"_default"}{"read"};
	my $post = $group_map{"_default"}{"post"};

	if (defined($attributes{"username"})) {
		my $username = $attributes{"username"} =~ s/@.*//r;
		my @groups = get_groups($username);
		if (@groups) {
			foreach my $group (@groups) {
				next unless defined($group_map{$group});
				my %rules = %{ $group_map{$group} };
				if (defined($rules{"read"})) {
					$read .= "," . $rules{"read"};
				}
				if (defined($rules{"post"})) {
					$post .= "," . $rules{"post"};
				}
			}
		}
	}

	$return_hash{"read"} = $read;
	$return_hash{"post"} = $post;

	return %return_hash;
}
