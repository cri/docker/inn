#!/bin/bash

runas_news() {
	su -s /bin/sh news -c "$*"
}


conf() {
	sed -i "s/\\\${INN_DOMAIN}/${INN_DOMAIN}/" /etc/news/inn.conf

	for secret in key.pem ldap_bind_password; do
		if [ -f "/secrets/$secret" ]; then
			cp "/secrets/$secret" "/etc/news/$secret" || continue
			chmod 0660 "/etc/news/$secret"
			chown news:news "/etc/news/$secret"
		fi
	done
}

init() {
	PATHDB=$(/usr/lib/news/bin/innconfval pathdb)
	if [ -z "$PATHDB" ]; then
		echo "Cannot determine the database path, aborting."
	else
		cd "$PATHDB" || return
		if [ ! -f active ]; then
			grep -v local /usr/lib/news/examples/active > ./active
		fi
		mkdir -p /var/spool/news/{articles,overview,incoming}/
		chown news:news /var/spool/news/* /var/lib/news/*
		echo "Building history database in $PATHDB... "
		runas_news /usr/lib/news/bin/makehistory -O
		runas_news /usr/lib/news/bin/makedbz -i -o -s 300000
      fi
}

syslog() {
	service rsyslog start
	touch /var/log/syslog
	chown syslog:syslog /var/log/syslog
	tail -f /var/log/syslog &
}

initial() {
	echo "Waiting for innd to start..."
	while [ ! -f /run/news/innd.pid ]; do
		echo "Still waiting..."
		sleep 2
	done

	echo "Creating initial groups... "
	grep -vE '^\s*#' /etc/news/initial | sort -u | while read -r group; do
		echo -n "  -> $group... "
		ctlinnd newgroup "$group"
	done
	echo "All initial groups have been created."
}

CMD=$1
shift

conf

if [ "$CMD" = "innd" ]; then
	initial &
	syslog
	echo "Starting innd process..."
	exec su -s /bin/sh news -c "/usr/lib/news/bin/innd -f $*"
fi

if [ "$CMD" = "nnrpd" ]; then
	syslog
	echo "Starting nnrpd process..."
	exec su -s /bin/sh news -c "/usr/lib/news/bin/nnrpd -D -f $*"
fi

if [ "$CMD" = "init" ]; then
	init
else
	exec su -s /bin/sh news -c "$CMD $*"
fi
